using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Apis.Drive.v3.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Test_Authorization.Pages.Admin
{
    public class AdminPageModel : PageModel
    {
        private UserManager<IdentityUser> _userManager;
        public IEnumerable<IdentityUser> Users { get; set; }

        public AdminPageModel(UserManager<IdentityUser> userManager)
		{
            _userManager = userManager;
		}
        public void OnGet()
        {
            Users = _userManager.Users;
        }
        public async Task<IActionResult> OnPostAsync(string id)
		{
            IdentityUser user = await _userManager.FindByIdAsync(id);
            if (user != null)
                await _userManager.DeleteAsync(user);

            return RedirectToPage();
		}
    }
}
