using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Test_Authorization.Models;

namespace Test_Authorization.Pages
{
    public class IndexModel : PageModel
    {
        private readonly SignInManager<User> _signInManager;

		public IndexModel(SignInManager<User> signInManager)
		{
			_signInManager = signInManager;
		}

		public async Task<IActionResult> OnPost()
		{
			await _signInManager.SignOutAsync();
			return RedirectToPage("/Index");
		}
	}
}
