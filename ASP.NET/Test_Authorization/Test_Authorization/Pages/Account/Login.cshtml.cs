using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using Test_Authorization.Models;
using Google.Apis.Auth.AspNetCore3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;

namespace Test_Authorization.Pages.Account
{
	public class LoginModel : PageModel
	{
		private readonly SignInManager<User> _signInManager;

		[BindProperty]
		[Required]
		[Display(Name = "�����")]
		public string Login { get; set; }

		[BindProperty]
		[Required]
		[DataType(DataType.Password)]
		[Display(Name = "������")]
		public string Password { get; set; }

		[BindProperty]
		[Display(Name = "���������?")]
		public bool RemembeMe { get; set; }

		public LoginModel(SignInManager<User> signInManager)
		{
			_signInManager = signInManager;
		}

		public async Task<IActionResult> OnPostAsync() 
		{
			//string path = @"D:\repository\gitlab\Test_Authorization\Test_Authorization\Data.json";
			//dynamic listUser;
			//using (StreamReader sr = new StreamReader(path))
			//{
			//	path = sr.ReadToEnd();
			//	listUser = JsonConvert.DeserializeObject(path);
			//}

			if (ModelState.IsValid)
			{
				var result = await _signInManager.PasswordSignInAsync(Login, Password, RemembeMe, false);

				if (result.Succeeded)
				{
					return RedirectToPage("/Index");
				}
				ModelState.AddModelError("", "������������ ����� ��� ������");
			}
			return Page();
		}		

		
	}
}
