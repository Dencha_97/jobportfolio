﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParsingMVC.Models
{
	public class InformationModel
	{
		public string Messange { get; set; } 
		public string FullMessange { get; set; }
		public bool CheckGoodLink { get; set; } = false;
		public bool CheckGoodDiv { get; set; } = false;
		public bool CheckGoodTable { get; set; } = false;
		public string GetClassTableLink() => CheckGoodLink == false ? "none" : "block";
		public string GetClassDiv() => CheckGoodDiv == false ? "none" : "block";
		public string GetClassTable() => CheckGoodTable == false ? "none" : "block";
		public char Delimiter { get; set; }
	}
}
