﻿

namespace ParsingMVC.Models
{
	public class CSVColumns 
	{
		public string CadNumber { get; set; }
		public string Category { get; set; }
		public string Vri { get; set; }
		public string Address { get; set; }
		public string CopyrightHolderFIO { get; set; }
		public string RightNumber { get; set; }
		public string BirthDate { get; set; }
		public string CopyrightHolderAddress { get; set; }
		public string Email { get; set; }
		public string CopyrightHolderDetails { get; set; }
		public string Snils { get; set; }
		public string RestrictionOfRights { get; set; }
		public string Lessee { get; set; }
	}
}
