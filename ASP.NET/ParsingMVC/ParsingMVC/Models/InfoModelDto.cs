﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParsingMVC.Models
{
	public class InfoModelDto : InformationModel
	{

		public List<string> ListResult = new List<string>();

		public List<string> ListDownLink = new List<string>();
	}
}
