﻿using System;
using System.Text;

namespace TicTacToe
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("-------------------- Игра -----------------");
			Console.WriteLine("----------- Крестики - нолики -----------\n");

			Console.WriteLine(GetPrintableState());

			while (logicGames.GetWinner() == Winner.GameIsUndefinished)
			{
				int index = int.Parse(Console.ReadLine());
				logicGames.MakeMove(index);

				Console.WriteLine();
				Console.WriteLine(GetPrintableState());
			}

			Console.WriteLine($"Результат: {logicGames.GetWinner()}");
		}

		private static LogicGames logicGames = new LogicGames();
		private static string GetPrintableState()
		{
			var sb = new StringBuilder();
			for (int i = 1; i < 9; i += 3)
			{
				sb.AppendLine("|     |     |     |")
					.AppendLine($"|  {GetPrintableChar(i)}  |  {GetPrintableChar(i + 1)}  |  {GetPrintableChar(i + 2)}  |")
					.AppendLine("|_____|_____|_____|");
			}

			return sb.ToString();
		}

		private static string GetPrintableChar(int i)
		{
			State state = logicGames.GetState(i);

			if (state == State.Unset)
				return i.ToString();
			return state == State.Cross ? "X" : "O";
		}
	}
}
