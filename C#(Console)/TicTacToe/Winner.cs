﻿namespace TicTacToe
{
	public enum Winner
	{
		Crosses, //Победа - Х
		Zeroes, //Победа - О
		Draw, //ничья
		GameIsUndefinished //игра не закончилась 
	}
}