﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuessNumber
{
	class Player
	{
		//загаданное число
		public int Number { get; set; }
		//число попыток
		public int Attempt { get; private set; }
		

		public Player()
		{
			Attempt = 5;

		}

		public void Search(int number)
		{
			Number = number;
			int nowNumber; //текущее число
			int max; //максимальное число диапазона
			int min; //минимальное число диапазона

			Random random = new Random();
			int randomNumber = random.Next(10);

			max = Number + randomNumber;
			min = Number - randomNumber;

			max = max > 100 ? 100 : max;
			min = min < 0 ? 0 : min;

			Console.WriteLine($"Число находится в диапазоне {min} - {max} ");

			do
			{
				Console.WriteLine($"Осталось попыток: {Attempt}");
				Console.Write("Число: ");
				nowNumber = Convert.ToInt32(Console.ReadLine());

				if (nowNumber < Number)
				{
					Attempt--;
				}
				else if(nowNumber > Number)
				{
					Attempt--;
				}
				else if(nowNumber == Number)
				{
					Console.WriteLine($"\nПоздравляю,Ты, угадал число! Это число: {Number}\n");
					Attempt = 5;
					break;
				}

				if (Attempt == 0)
				{
					Console.WriteLine("\nТы проиграл. \n" +
					                  "Попробуй еще раз, заново!\n");
					randomNumber = random.Next(10);

					max = Number + randomNumber;
					min = Number - randomNumber;

					max = max > 100 ? 100 : max;
					min = min < 0 ? 0 : min;

					Console.WriteLine($"Число находится в диапазоне {min} - {max} ");
					Attempt = 5;
				}

			} while (true);
		}
	}
}
