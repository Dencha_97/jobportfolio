﻿using System;
using System.IO;
using System.Linq;
using Telegram.Bot;
using Telegram.Bot.Args;

namespace IntensiveBottelegram
{
	class Program
	{
		private static TelegramBotClient Bot;

		static void Main(string[] args)
		{
			string token = "1650481710:AAGKJQAqR_RI6V883k3iQKXzl6s0UV8ehIk";
			Bot = new TelegramBotClient(token);

			Bot.OnMessage += BotOnOnMessage;
			Bot.StartReceiving();

			Console.ReadLine();
		}

		private static void BotOnOnMessage(object? sender, MessageEventArgs e)
		{
			var chatId = e.Message.Chat.Id; //откуда пришло
			Bot.SendTextMessageAsync(chatId, "Hello");
		}
	}

	class Quiz
	{
		public Quiz()
		{
			var lines = File.ReadAllLines("data.txt");

			var questions = lines.Select(s => s.Split('|'))
								.Select(s => 
								new QuestionItem
								{
									Question = s.[0],
									Answer = s.[1]
								})
								.ToList();

		}
	}


	class QuestionItem
	{
		public string Question { get; set; }
		public string Answer { get; set; }
	}
}
