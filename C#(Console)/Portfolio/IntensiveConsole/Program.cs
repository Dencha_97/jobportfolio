﻿using System;
using System.IO;
using System.Linq;

namespace IntensiveConsole
{
	class Program
	{
		static void Main(string[] args)
		{
			var lines = File.ReadAllLines("data.txt");

			var questions = lines.Select(s => s.Split('|'))
								.Select(s => (s[0], s[1])) //кортеж
								.ToList();

			int score = 0;
			var random = new Random();
			int count = questions.Count;
			

			while (true)
			{
				if(count < 1)
				{
					count = questions.Count;
				}

				var index = random.Next(count - 1);
				var question = questions[index];

				questions.RemoveAt(index);
				questions.Add(question);
				count--;

				int opened = 0;
				
				while (opened < question.Item2.Length)
				{
					string answer = question.Item2
					.Substring(0, opened)
					.PadRight(question.Item2.Length, '_');

					Console.ForegroundColor = ConsoleColor.Blue;
					Console.WriteLine($"{question.Item1}: {question.Item2.Length} букв");
					Console.ForegroundColor = ConsoleColor.DarkYellow;
					Console.WriteLine(answer);

					Console.ForegroundColor = ConsoleColor.Magenta;
					var tryAnswer = Console.ReadLine();

					if (tryAnswer.Trim().ToLowerInvariant() == question.Item2.Trim().ToLowerInvariant()) //Trim() - обрезает все пробелы, а ToLowerInvariant() - приводит все буквы в нижний регистр
					{
						score++;
						Console.ForegroundColor = ConsoleColor.Green;
						Console.WriteLine($"Молорик! Правильно! Это - {question.Item2}");
						Console.WriteLine($"У Вас {score} очков\n");
						opened = -1;
						break;
					}
					else
					{
						opened++;
					}
				}
				if(opened != -1)
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.WriteLine($"Никто не отгадал! Это было - {question.Item2}\n");
				}
			}

		}
	}
}
