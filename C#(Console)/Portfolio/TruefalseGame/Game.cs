﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace TrueFalseGame
{
	class Game
	{
		private readonly List<Question> question;
		private readonly int allowedMistakes;
		private int counter;
		private int mistakes;

		public GameStatus GameStatus { get; private set; }

		public event EventHandler<GameResultEventArgs> EndOfGame;

		public Game(string filePath, int allowedMistakes = 2)
		{
			if (filePath == null)
				throw new ArgumentNullException("filePath");

			if (filePath == "")
				throw new ArgumentNullException("filePath путь пуст");

			if (allowedMistakes < 2)
				throw new ArgumentException("allowedMistakes shiuld be >= 2");

			List<Question> question = File.ReadAllLines(filePath)
				.Select(x =>
				{
					string[] parts = x.Split(';');
					string text = parts[0];
					bool correctAnswer = parts[1] == "Yes";
					string explanation = parts[2];

					return new Question(text, correctAnswer, explanation);
				}).ToList();

			this.question = question;
			this.allowedMistakes = allowedMistakes;
			GameStatus = GameStatus.GameInProgress;
		}

		public Question GetNextQuestion()
		{
			return question[counter];
		}

		public void GiveAnswer(bool answer)
		{
			if (question[counter].CorrectAnswer != answer)
				mistakes++;

			if (counter == question.Count - 1 || mistakes > allowedMistakes)
			{
				GameStatus = GameStatus.GameIsOver;
				if (EndOfGame != null)
					EndOfGame(this, new GameResultEventArgs(counter, mistakes, mistakes <= allowedMistakes));
			}

			counter++;
		}
	}
}