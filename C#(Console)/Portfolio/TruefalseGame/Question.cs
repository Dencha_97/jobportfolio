﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrueFalseGame
{
	class Question
	{
		public string Text { get; set; }
		public bool CorrectAnswer { get; set; }
		public string Explanation { get; set; }
		
		public Question(string text, bool correctAnswer, string explanation)
		{
			Text = text;
			CorrectAnswer = correctAnswer;
			Explanation = explanation;
		}
	}
}
