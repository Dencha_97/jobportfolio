﻿namespace TrueFalseGame
{
	public enum GameStatus
	{
		GameIsOver,
		GameInProgress
	}
}