﻿using System;

namespace TrueFalseGame
{
	class Program
	{
		static void Main(string[] args)
		{
			var game = new Game("Questions.csv");

			game.EndOfGame += (sender, eventArgs) =>
			{
				Console.WriteLine($"Questions asked: {eventArgs.QuestionsPassed}.");
				Console.WriteLine($"Mistakes made:{eventArgs.MistakesMade}");
				Console.WriteLine(eventArgs.IsWin ? "You win!" : "You lose!");
			};

			while (game.GameStatus == GameStatus.GameInProgress)
			{
				Question q = game.GetNextQuestion();
				Console.WriteLine("Dou you belive in the next statemebt or question? Enter 'y' or 'n'");
				Console.WriteLine(q.Text);

				string answer = Console.ReadLine();
				bool boolAnswer = answer == "y";

				if(q.CorrectAnswer == boolAnswer)
					Console.WriteLine("Good job. You're right!");
				else
				{
					Console.WriteLine("Oooops, actually you're mistakes. Here is the commentary");
					Console.WriteLine(q.Explanation);
				}
				game.GiveAnswer(boolAnswer);
			}

			
		}
	}
}