﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2
{
	class One
	{
		//1.Реализовать простой калькулятор. Система запрашивает знак
		//операции, затем первый операнд и второй операнд. Выводится результат вычисления.
		public static void Method()
		{
			char sign;
			int n1, n2, result;

			Console.Write("Введите знак \"+\",\"-\",\"*\",\"/\": ");
			sign = Convert.ToChar(Console.ReadLine());

			Console.Write("Введите первое число: ");
			n1 = Convert.ToInt32(Console.ReadLine());
			Console.Write("Введите первое число: ");
			n2 = Convert.ToInt32(Console.ReadLine());

			switch (sign)
			{
				case '+':
					result = n1 + n2;
					Console.WriteLine("Сумма равна: " + result);
					break;
				case '-':
					result = n1 - n2;
					Console.WriteLine("Разность равна: " + result);
					break;
				case '*':
					result = n1 * n2;
					Console.WriteLine("Произведение равна: " + result);
					break;
				case '/':
					result = n1 / n2;
					Console.WriteLine("Деление равна: " + result);
					break;
				default:
					Console.WriteLine("Выбран не тот знак");
					break;
			}
		}		
	}
}


