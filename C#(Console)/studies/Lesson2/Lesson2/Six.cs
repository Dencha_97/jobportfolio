﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2
{
	class Six
	{
		public static void Method()
		{
			//6 *.Реализовать программу, которая запрашивает в качестве входных данных число и степень. На выходе возведения введенного числа в степень. 
			//Степень может быть отрицательная.
			//Примечание: Использовать рекурсию.
			int number;    //число
			int degree; //степень

			Console.Write("Введите число: ");
			number = Convert.ToInt32(Console.ReadLine());
			Console.Write("Введите степень: ");
			degree = Convert.ToInt32(Console.ReadLine());
			Console.WriteLine(Recurs(number, degree));
		}

		static int Recurs(int n, int d)
		{
			if (d == 0)
				return 1;
			else if (d < 0)
				return 1 / (n * Recurs(1 / n, d + 1));
			return n * Recurs(n, d - 1);

		}
	}
}
