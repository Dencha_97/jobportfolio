﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
	class Program
	{
		static void Main(string[] args)
		{
			int numberTask = 0;
			do
			{
				Console.Write("Выберете задачу от 1 до 6: ");
				numberTask = Convert.ToInt32(Console.ReadLine());
				switch (numberTask)
				{
					case 1:
						One.Method();
						break;
					case 2:
						Two.Method();
						break;
					case 3:
						Three.Method();
						break;
					case 4:
						Four.Method();
						break;
					case 5:
						Five.Method();
						break;
					case 6:
						Six.Method();
						break;
					default:
						Console.WriteLine("Такой задачи нет");
						break;
				}
			} while (true);

			Console.ReadKey();
		}
	}
}
