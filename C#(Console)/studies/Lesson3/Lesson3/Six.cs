﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
	class Six
	{
		/*
		 6*. Реализовать игру “Сапер”. Игрок определяет уровень от 1 до 5.
			Уровень обозначает количество мин на поле, умноженное на 2.
			На нем расставляются мины случайным образом в количестве от 2 до
			10. Отображается поле 5 х 5 в виде стандартной матрицы, где ячейка условный
			знак (например: ‘*’, или ‘’, или ‘o’ не имеет значения).
			Начинается игра. Игрок вводит позицию ячейки.
			Если на этой ячейке не было мины новая попытка.
			Игрок побеждает, когда он откроет не менее 2/3 от безопасных полей.
			Игрок проигрывает, если случайно попадет на 2 мины.
			После каждого хода матрица обновляется (“перерисовывается”), где
			вместо исходного символа в месте очередного выбора, ставится
			символ соответствующий текущему состоянию этой клетки.
			Если игрок указывает на уже открытую ячейку, то вывод сообщения
			на экран, что ячейка уже открыта и запрос повторного ввода ячейки
			для проверки.
			Примечание: В качестве типа матрицы использовать перечисление
			(enum). А его элементы должны соответствовать контексту
			значений ячеек (то есть “не проверена”, “безопасная”, “мина”). Для
			случайного расположение“мин” использовать класс Random (см.
			MSDN)
		 */

		enum MyEnum
		{
			не_проверена,
			безопасная,
			мина
		};

		private static int _level;

		public static void Method()
		{
			
			int countHeart = 2;

			do
			{
				try
				{
					Console.Write("Введите уровень (1-5): ");
					_level = Int32.Parse(Console.ReadLine());
					ProcessGame(ref countHeart);
				}
				catch (Exception)
				{
					Console.WriteLine("Произошла ошибка, повторите ввод");
				}
			} while (true);
		}

		private static int LevelOfMine()
		{
			switch (_level)
			{
				case 1:
					return 4;
					break;
				case 2:
					return 8;
					break;
				case 3:
					return 12;
					break;
				case 4:
					return 16;
					break;
				case 5:
					return 20;
					break;
				default:
					return 0;
					break;
			}
		}

		private static void ProcessGame(ref int countHeart)
		{
			PloshadkaSchema();
			int countMine = LevelOfMine();
			int countFree = 0;
			bool flagLosing = false;
			string[,] arrayPloshdka = Ploshadka();
			string[,] arrayMina = FormationMines(countMine);
												
			do
			{
				Console.WriteLine("\nВведите позицию:");
				try
				{
					LogicGame(arrayPloshdka, arrayMina, ref countFree, ref countMine, ref countHeart, ref flagLosing);
				}
				catch (Exception)
				{
					Console.WriteLine("Произошла ошибка, повторите ввод");
				}
				

				if (flagLosing)
				{
					countHeart = 2;
					Console.WriteLine("Жизни кончились. Конец Игры!\n");
					return;
				}

				if (Winner(countFree))
				{
					Console.WriteLine("Победа! Конец игры!\n");
					return;
				}

			} while (true);
		}
		
		//метод ввода позиции
		private static void EnterPosition(ref int stroka, ref int stolbec)
		{
			Console.Write("Номер позиции в строке (0-4): ");
			stroka = Int32.Parse(Console.ReadLine());

			Console.Write("Номер позиции в столбце (0-4): ");
			stolbec = Int32.Parse(Console.ReadLine());
		}

		//расчеты победы
		private static bool Winner(int count)
		{
			int numberWin;

			switch (_level)
			{
				case 1:
					numberWin = 21 * 2 / 3;

					if (count == numberWin)
						return true;
					break;
				case 2:
					numberWin = 17 * 2 / 3;

					if (count == numberWin)
						return true;
					break;
				case 3:
					numberWin = 13 * 2 / 3;

					if (count == numberWin)
						return true;
					break;
				case 4:
					numberWin = 9 * 2 / 3;

					if (count == numberWin)
						return true;
					break;
				case 5:
					numberWin = 5 * 2 / 3;

					if (count == numberWin)
						return true;
					break;
				default:
					Console.WriteLine("Введен не правильно уровень, повторите попытку");
					break;
			}	

			return false;
		}

		//процесс игры
		private static void LogicGame(string[,] arrayPloshdka, string[,] arrayMina, ref int countFree, ref int countMine, ref int countHeart, ref bool flagLosing)
		{
			int stroka = 0, stolbec = 0;
			EnterPosition(ref stroka, ref stolbec);

			for (int i = 0; i < arrayPloshdka.GetLength(0); i++)
			{
				for (int j = 0; j < arrayPloshdka.GetLength(1); j++)
				{
					if (arrayMina[stroka, stolbec] != "#")
					{
						if(arrayPloshdka[stroka, stolbec] == "0")
						{
							Console.WriteLine("Безопасная ячейка уже открыта");
							return;
						}
							
						countFree++;
						Console.WriteLine($"Мины нет, Count {countFree}");
						Ploshadka(arrayPloshdka, stroka, stolbec, "0");
						break;
					}
					else
					{
						if (arrayPloshdka[stroka, stolbec] == "#")
						{
							Console.WriteLine("Ячейка c миной уже открыта");
							return;
						}

						Console.WriteLine($"Мина. Остолось попыток {--countHeart}");
						Ploshadka(arrayPloshdka, stroka, stolbec, "#");

						if (countHeart == 0)
						{
							flagLosing = true;							
							return;
						}
						break;
					}
				}
				break;
			}			
		}
		
		//формирования количества мин
		private static string[,] FormationMines(int countMine)
		{
			string[,] arrayMina = new string[5, 5];

			Random random = new Random();
			int r, c;

			for (int i = 0; i < arrayMina.GetLength(0); i++)
			{
				for (int j = 0; j < arrayMina.GetLength(1); j++)
				{
					if (countMine > 0)
					{
						r = random.Next(5);
						c = random.Next(5);
						arrayMina[r, c] = "#";
						countMine--;
					}
				}
			}
			return arrayMina;
		}

		//создание матрицы 5х5
		private static string[,] Ploshadka()
		{
			string[,] arrayPloshdka = new string[5, 5];
			for (int i = 0; i < arrayPloshdka.GetLength(0); i++)
			{
				for (int j = 0; j < arrayPloshdka.GetLength(1); j++)
				{
					arrayPloshdka[i, j] = "*";
				}
			}
			return arrayPloshdka;
		}

		//изменения в матрицу
		private static void Ploshadka(string[,] arrayPloshdka, int stroka, int stolbec, string v)
		{
			for (int i = 0; i < arrayPloshdka.GetLength(0); i++)
			{
				for (int j = 0; j < arrayPloshdka.GetLength(1); j++)
				{
					if (i == stroka && j == stolbec) 
						arrayPloshdka[i, j] = v;

					Console.Write($"{arrayPloshdka[i, j]} | ");
				}
				Console.WriteLine();
			}
		}

		//вывод матрицы на экран
		private static void PloshadkaSchema()
		{
			for (int i = 0; i < Ploshadka().GetLength(0); i++)
			{
				for (int j = 0; j < Ploshadka().GetLength(1); j++)
				{
					Console.Write($"{Ploshadka()[i, j]} | ");
				}
				Console.WriteLine();
			}
		}
	}
}