﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
	class Four
	{
        /*
		 4. Изучить исходник bubblesort.cs, демонстрирующий ручную
			сортировку массива алгоритмом типа “Сортировка пузырьком”.
			Модифицировать код таким образом, чтобы сортировка ранжировала
			(упорядочивала) массив не по возрастанию, а по убыванию и лишь с
			той частью массива, т.е. с его подпоследовательностью, которая не
			включает в себя первый (“нулевой”) и последний элементы. (Пример:
			1 9 2 5 7 сортировать надо только 9 2 5)
		 */

        static public void Method()
		{
            Random rnd = new Random();
            int[] mas = new int[10];
			int[] masSort = new int[8];

			for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = rnd.Next(1, 10);
            }

            foreach (int i in mas)
            {
                Console.Write(i + ", ");
            }
            Console.WriteLine(" - ДО ");

			for (int i = 1; i < mas.Length-1; i++)
			{
				masSort[i - 1] = mas[i];
			}
            
            for (int i = 0; i < masSort.Length-1; i++)
            {
                for (int j = i + 1; j < masSort.Length; j++)
                {
                    if (masSort[i] < masSort[j]) 
                    {                        
                        int tmp = masSort[i];
						masSort[i] = masSort[j];
						masSort[j] = tmp;     
                    }
                }
            }

            foreach (int i in masSort)
            {
                Console.Write(i + ", ");
            }
            Console.Write(" - ПОСЛЕ");
			Console.WriteLine();
        }
	}
}
