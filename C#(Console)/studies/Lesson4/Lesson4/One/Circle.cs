﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4.One
{
	/*
	 1. Реализовать класс “Окружность”. Позволяет выводить
		информацию об окружности (радиус, координаты точки). Методы
		увеличения/уменьшения в n раз.
		А также вывод свойств: площадь и т.д. Иметь ввиду, что в качестве
		членов использовать только однозначно необходимые для описания
		окружности поля.
	 */
	class Circle
	{
		int radius;
		int dot; //точка координат
		Random r = new Random();

		public Circle()
		{
			radius = r.Next(1,10);
			dot = r.Next(1, 10);
		}

		//увеличение
		public int Increase(int n)
		{
			return radius*n;
		}

		//уменьшение
		public int Decrease(int n)
		{
			return radius / n;
		}
		public void Snow()
		{
			Console.WriteLine("Радиус: " + radius + " , точка доступа: " + dot);
		}
	}

}
