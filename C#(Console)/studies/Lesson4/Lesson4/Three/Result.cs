﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4.Three
{
	class Result
	{
		static public void Method()
		{
			double n1, n2;
			char sign = ' ';
			int i = 0;
			List<double> list = new List<double>();
			Calculator calc = new Calculator(0, 0);
			do
			{
				try
				{

					Console.Write("Введите первое число: ");
					n1 = Convert.ToInt32(Console.ReadLine());
					Console.Write("Введите второе  число: ");
					n2 = Convert.ToInt32(Console.ReadLine());

					calc = new Calculator(n1, n2);

					Console.Write("Введите знак: ");
					sign = Convert.ToChar(Console.ReadLine());
					
					list.Add(calc.Operation(sign));
					Console.WriteLine(n1 + " + " + n2 + " = " + calc.Operation(sign));

				}
				catch (Exception e)
				{
					Console.WriteLine(e.Message);
				}

				i++;
				Console.WriteLine();
			} while (i < 3);

			foreach (var item in list)
			{
				Console.WriteLine(item);
			}
			

		}

	}
}