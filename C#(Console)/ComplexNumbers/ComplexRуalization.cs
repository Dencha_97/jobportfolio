﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComplexNumbers
{
	public class ComplexRуalization
	{
		//св-во вещественной части
		public double Material { get; set; }

		//св-во мнимой части
		public double Imaginary { get; set; }

		public ComplexRуalization(double m, double i)
		{
			Material = m;
			Imaginary = i;
		}

		private  ComplexRуalization()
		{
		}

		/// <summary>
		/// Сложение комплексного числа
		/// </summary>
		/// <param name="cr"></param>
		/// <returns></returns>
		public ComplexRуalization Plus(ComplexRуalization cr)
		{
			var complex = new ComplexRуalization();

			complex.Material = cr.Material + Material;
			complex.Imaginary = cr.Imaginary + Imaginary;

			return complex;
		}

		/// <summary>
		/// Вычитание комплексного числа
		/// </summary>
		/// <param name="cr"></param>
		/// <returns></returns>
		public ComplexRуalization Minus(ComplexRуalization cr)
		{
			var complex = new ComplexRуalization();

			complex.Material = cr.Material - Material;
			complex.Imaginary = cr.Imaginary - Imaginary;

			return complex;
		}
	}
}
