﻿using System;

namespace ComplexNumbers
{
	class Program
	{
		static void Main(string[] args)
		{
			ComplexRуalization complex1 = new ComplexRуalization(1, 2);
			ComplexRуalization complex2 = new ComplexRуalization(3, 1);

			ComplexRуalization complexPlus = complex1.Plus(complex2);

			Console.WriteLine($"Результат: Вещественная часть: {complexPlus.Material}, Мнимая часть {complexPlus.Imaginary}");

			ComplexRуalization complexMinus = complex1.Minus(complex2);

			Console.WriteLine($"Результат: Вещественная часть: {complexMinus.Material}, Мнимая часть {complexMinus.Imaginary}");
		}
	}
}
